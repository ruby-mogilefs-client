# -*- encoding: binary -*-

# internal compatibility class for older Rubies
module MogileFS::CopyStream # :nodoc:
  @r_args = IO::RDONLY | IO::NOCTTY
  @w_args = [ IO::WRONLY|IO::CREAT|IO::NOCTTY|IO::TRUNC, 0666 ]
  def self.copy_stream(src, dst)
    src_io = src.respond_to?(:to_str) ? File.open(src, @r_args) : src
    dst_io = dst.respond_to?(:to_str) ? File.open(dst, *@w_args) : dst
    buf = ""
    written = 0
    if src_io.respond_to?(:readpartial)
      begin
        src_io.readpartial(0x4000, buf)
        written += dst_io.write(buf)
      rescue EOFError
        break
      end while true
    else
      while src_io.read(0x4000, buf)
        written += dst_io.write(buf)
      end
    end
    dst_io.flush if dst_io.respond_to?(:flush)
    written
  ensure
    src_io.close if src.respond_to?(:to_str)
    dst_io.close if dst.respond_to?(:to_str)
  end
end
