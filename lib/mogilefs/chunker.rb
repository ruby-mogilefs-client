# -*- encoding: binary -*-
class MogileFS::Chunker
  attr_reader :io

  def initialize(io, md5, expect_md5)
    @io = io
    @md5 = md5
    @expect_md5 = expect_md5
  end

  def write(buf)
    rv = buf.bytesize
    @io.write("#{rv.to_s(16)}\r\n")
    @io.write(buf)
    @md5.update(buf) if @md5
    @io.write("\r\n".freeze)
    rv
  end

  def flush
    if @md5
      content_md5 = [ @md5.digest ].pack('m').rstrip!
      if @expect_md5.respond_to?(:call)
        expect = @expect_md5.call.strip
        if expect != content_md5
          raise MogileFS::ChecksumMismatchError,
            "expected: #{expect.inspect} actual: #{content_md5.inspect}"
        end
      end
      @io.write("0\r\nContent-MD5: #{content_md5}\r\n\r\n")
    else
      @io.write("0\r\n\r\n".freeze)
    end
  end
end
