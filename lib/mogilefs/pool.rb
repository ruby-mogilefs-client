# -*- encoding: binary -*-
require 'thread'

class MogileFS::Pool

  # Must be a positive Integer that is greater than :purge_keep
  # Default: 5
  attr_accessor :purge_threshold

  # Must be a positive Integer that is smaller than :purge_threshold
  # Default: 2
  attr_accessor :purge_keep

  BadObjectError = Class.new(RuntimeError)

  def initialize(klass, *args)
    @args = args
    @klass = klass
    @queue = Queue.new
    @objects = {}
    @purge_threshold = 5
    @purge_keep = 2
  end

  def get
    @queue.pop true
  rescue ThreadError
    object = @klass.new(*@args)
    @objects[object] = object
  end

  def put(o)
    raise BadObjectError unless @objects.include? o
    @queue.push o
    purge
  end

  def use
    object = get
    yield object
    nil
  ensure
    put object
    nil
  end

  def purge
    return if @queue.length < @purge_threshold
    begin
      until @queue.length <= @purge_keep
        obj = @queue.pop true
        @objects.delete obj
        obj.backend.shutdown if MogileFS::Client === obj
      end
    rescue ThreadError
    end
  end

end

