# -*- encoding: binary -*-

module MogileFS::Util

  # MogileFS::Util::StoreContent allows you to roll your own method
  # of streaming data on an upload (instead of using a string or file)
  #
  # Current versions of this library support streaming a IO or IO-like
  # object to using MogileFS::MogileFS#store_file, so using StoreContent
  # may no longer be necessary.
  class StoreContent < Proc
    def initialize(total_size, &writer_proc)
      @total_size = total_size
      super(&writer_proc)
    end
    def length
      @total_size
    end
  end
end

require 'timeout'
##
# Timeout error class.  Subclassing it from Timeout::Error is the only
# reason we require the 'timeout' module, otherwise that module is
# broken and worthless to us.
MogileFS::Timeout = Class.new(Timeout::Error)
