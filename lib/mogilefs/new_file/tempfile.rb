# -*- encoding: binary -*-
# here are internal implementation details, do not rely on them in your code
require 'tempfile'
require 'mogilefs/http_file'

class MogileFS::NewFile::Tempfile < Tempfile
  def initialize(*args)
    @mogilefs_httpfile_args = args
    super("mogilefs-client")
    unlink
  end

  def commit
    rewind
    tmp = MogileFS::HTTPFile.new(*@mogilefs_httpfile_args)
    tmp.big_io = to_io
    tmp.commit
  end

  def close
    commit
    super
  end
end
