require 'test/unit'

class TestNhpCompat < Test::Unit::TestCase

  def test_nhp_compat
    ver_test_nhp('~> 2.9')
    ver_test_nhp('~> 3.0')
  end

  def ver_test_nhp(verspec)
    pid = fork do
      begin
        gem 'net-http-persistent', verspec
      rescue LoadError => e
        warn "SKIPPING net-http-persistent #{verspec}\n" \
             "#{e.message} (#{e.class})\n"
      end
      require 'mogilefs'
      mg = MogileFS::MogileFS.new :hosts => %w(127.0.0.1:7500), :domain => 'x'
      exit!(Net::HTTP::Persistent === mg.nhp_new('foo'))
    end
    _, status = Process.waitpid2(pid)
    assert status.success?, status.inspect
  end
end
