# -*- encoding: binary -*-
require './test/fresh'

class TestMogileFSIntegrationListKeys < Test::Unit::TestCase
  include TestFreshSetup

  def setup
    setup_mogilefs
    add_host_device_domain
    @client = MogileFS::MogileFS.new(:hosts => @trackers, :domain => @domain)
  end

  alias teardown teardown_mogilefs

  def test_list_keys
    k = %w(a b c d e f g)
    k.each { |x| @client.store_content("lk_#{x}", nil, x) }
    expect = k.map { |x| "lk_#{x}" }
    rv = @client.list_keys
    assert_equal([ expect, expect.last ] , rv)
    nr = 0
    @client.list_keys do |key, length, devcount|
      assert_equal 1, length
      assert_kind_of Integer, devcount
      assert_equal expect[nr], key
      nr += 1
    end
  end

  def test_list_keys_strange
    @client.store_content("hello+world", nil, "HI")
    rv = @client.list_keys
    assert_equal "hello+world", rv[0][0]
  end

  def test_each_key
    9.times { |i| @client.store_content("ek_#{i}", nil, i.to_s) }
    n = 0
    @client.each_key do |key|
      assert_equal "ek_#{n.to_s}", key
      n += 1
    end
    assert_equal 9, n
  end

  def test_each_file_info
    9.times { |i| @client.store_content("ek_#{i}", nil, i.to_s) }
    n = 0
    @client.each_file_info do |info|
      assert_equal @client.domain, info["domain"]
      assert_equal n.to_s.size, info["length"]
      assert_kind_of Integer, info["fid"]
      assert_kind_of Integer, info["devcount"]
      assert_equal "default", info["class"]
      assert_equal "ek_#{n}", info["key"]
      n += 1
    end
    assert_equal 9, n
  end
end
