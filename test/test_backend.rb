# -*- encoding: binary -*-
require 'test/unit'
require './test/setup'

$TESTING = true

class MogileFS::Backend

  attr_accessor :hosts
  attr_reader :timeout, :dead
  attr_writer :lasterr, :lasterrstr, :socket

end

class TestBackend < Test::Unit::TestCase

  def setup
    @backend = MogileFS::Backend.new :hosts => ['localhost:1']
  end

  def test_initialize
    assert_raises ArgumentError do MogileFS::Backend.new end
    assert_raises ArgumentError do MogileFS::Backend.new :hosts => [] end
    assert_raises ArgumentError do MogileFS::Backend.new :hosts => [''] end

    assert_equal ['localhost:1'], @backend.hosts
    assert_equal 3, @backend.timeout
    assert_equal 3, @backend.instance_variable_get(:@connect_timeout)
    assert_equal nil, @backend.lasterr
    assert_equal nil, @backend.lasterrstr
    assert_equal({}, @backend.dead)

    @backend = MogileFS::Backend.new :hosts => ['localhost:6001'], :timeout => 1
    assert_equal 1, @backend.timeout
    assert_equal 1, @backend.instance_variable_get(:@connect_timeout)
  end

  def test_do_request
    srv = TCPServer.new("127.0.0.1", 0)
    port = srv.addr[1]
    accepted = Thread.new do
      client = srv.accept
      client.write("OK 1 you=win\r\n")
      client
    end
    @backend.hosts = [ "127.0.0.1:#{port}" ]
    assert_equal({'you' => 'win'},
                 @backend.do_request('go!', { 'fight' => 'team fight!' }))
    accepted = accepted.value
    assert_equal "go! fight=team+fight%21\r\n", accepted.readpartial(4096)
  end

  def test_automatic_exception
    assert ! MogileFS::Backend.const_defined?('PebkacError')
    assert @backend.error('pebkac')
    assert_equal MogileFS::Error, @backend.error('PebkacError').superclass
    assert MogileFS::Backend.const_defined?('PebkacError')

    assert ! MogileFS::Backend.const_defined?('PebKacError')
    assert @backend.error('peb_kac')
    assert_equal MogileFS::Error, @backend.error('PebKacError').superclass
    assert MogileFS::Backend.const_defined?('PebKacError')

    assert_equal MogileFS::Error, MogileFS::Backend::OMFGWTFBBQError.superclass
    assert_raises(NameError) do
      MogileFS::Backend::FailFailFail
    end
  end

  def test_make_request
    assert_equal "go! fight=team+fight%21\r\n",
                 @backend.make_request('go!', { 'fight' => 'team fight!' })
  end

  def test_parse_response
    assert_equal({'foo' => 'bar', 'baz' => 'hoge'},
                 @backend.parse_response("OK 1 foo=bar&baz=hoge\r\n"))

    err = nil
    begin
      @backend.parse_response("ERR you totally suck\r\n")
    rescue MogileFS::Error => err
      assert_equal 'MogileFS::Backend::YouError', err.class.to_s
      assert_equal 'totally suck', err.message
    end
    assert_equal 'MogileFS::Backend::YouError', err.class.to_s

    assert_equal 'you', @backend.lasterr
    assert_equal 'totally suck', @backend.lasterrstr

    assert_raises MogileFS::InvalidResponseError do
      @backend.parse_response 'garbage'
    end
    assert_raises MogileFS::InvalidResponseError do
      @backend.parse_response("OK 1 foo=bar&baz=hoge")
    end
  end

  def test_parse_response_newline
    begin
      @backend.parse_response("ERR you totally suck\r\n")
    rescue MogileFS::Error => err
      assert_equal 'MogileFS::Backend::YouError', err.class.to_s
      assert_equal 'totally suck', err.message
    end

    assert_equal 'you', @backend.lasterr
    assert_equal 'totally suck', @backend.lasterrstr
  end

  def test_readable_eh_not_readable
    srv = TCPServer.new("127.0.0.1", 0)
    port = srv.addr[1]
    @backend = MogileFS::Backend.new(:hosts => [ "127.0.0.1:#{port}" ],
                                     :timeout => 0.5)
    begin
      @backend.do_request 'foo', {}
    rescue MogileFS::UnreadableSocketError => e
      assert_match(/127\.0\.0\.1:#{port} never became readable/, e.message)
    rescue Exception => err
      flunk "MogileFS::UnreadableSocketError not raised #{err} #{err.backtrace}"
    else
      flunk "MogileFS::UnreadableSocketError not raised"
    end
  end

  def test_socket_dead
    assert_equal({}, @backend.dead)
    assert_raises(MogileFS::UnreachableBackendError) do
      @backend.do_request('test', {})
    end
    assert_equal(['localhost:1'], @backend.dead.keys)
  end

  def test_socket_robust_on_dead_server
    10.times do
      t1 = TCPServer.new("127.0.0.1", 0)
      t2 = TCPServer.new("127.0.0.1", 0)
      hosts = ["127.0.0.1:#{t1.addr[1]}", "127.0.0.1:#{t2.addr[1]}"]
      @backend = MogileFS::Backend.new(:hosts => hosts.dup)
      assert_equal({}, @backend.dead)
      t1.close
      thr = Thread.new do
        client = t2.accept
        client.write("OK 1 foo=bar\r\n")
        client
      end
      rv = @backend.do_request('test', { "all" => "ALL" })
      accepted = thr.value
      assert_equal "test all=ALL\r\n", accepted.readpartial(666)
      assert_equal({"foo"=>"bar"}, rv)
    end
  end

  def test_shutdown
    srv = TCPServer.new('127.0.0.1', 0)
    port = srv.addr[1]
    @backend = MogileFS::Backend.new :hosts => [ "127.0.0.1:#{port}" ]
    assert @backend.socket
    assert ! @backend.socket.closed?
    client = srv.accept
    client.write '1'
    resp = @backend.socket.read(1)
    @backend.shutdown
    assert_equal nil, @backend.instance_variable_get(:@socket)
    assert_equal 1, resp.to_i
  end

  def test_url_decode
    assert_equal({"\272z" => "\360opy", "f\000" => "\272r"},
                 @backend.url_decode("%baz=%f0opy&f%00=%bar"))
    assert_equal({}, @backend.url_decode(''))
  end

  def test_url_encode
    params = [["f\000", "\272r"], ["\272z", "\360opy"]]
    assert_equal "f%00=%bar&%baz=%f0opy", @backend.url_encode(params)
  end

  def test_url_escape # \n for unit_diff
    actual = (0..255).map { |c| @backend.url_escape c.chr }.join "\n"

    expected = []
    expected.push(*(0..0x1f).map { |c| "%%%0.2x" % c })
    expected << '+'
    expected.push(*(0x21..0x2b).map { |c| "%%%0.2x" % c })
    expected.push(*%w[, - . /])
    expected.push(*('0'..'9'))
    expected.push(*%w[: %3b %3c %3d %3e %3f %40])
    expected.push(*('A'..'Z'))
    expected.push(*%w[%5b \\ %5d %5e _ %60])
    expected.push(*('a'..'z'))
    expected.push(*(0x7b..0xff).map { |c| "%%%0.2x" % c })

    expected = expected.join "\n"

    assert_equal expected, actual
  end

  def test_url_unescape
    input = []
    input.push(*(0..0x1f).map { |c| "%%%0.2x" % c })
    input << '+'
    input.push(*(0x21..0x2b).map { |c| "%%%0.2x" % c })
    input.push(*%w[, - . /])
    input.push(*('0'..'9'))
    input.push(*%w[: %3b %3c %3d %3e %3f %40])
    input.push(*('A'..'Z'))
    input.push(*%w[%5b \\ %5d %5e _ %60])
    input.push(*('a'..'z'))
    input.push(*(0x7b..0xff).map { |c| "%%%0.2x" % c })

    actual = input.map { |c| @backend.url_unescape c }.join "\n"

    expected = (0..255).map { |c| c.chr }.join "\n"

    assert_equal expected, actual
  end

  def test_fail_timeout
    o = { :domain => "none", :hosts => %w(0:666 0:6 0:66) }
    c = MogileFS::MogileFS.new(o)
    assert_equal 5, c.backend.instance_variable_get(:@fail_timeout)
    o[:fail_timeout] = 0.666
    c = MogileFS::MogileFS.new(o)
    assert_equal 0.666, c.backend.instance_variable_get(:@fail_timeout)
  end

  def test_connect_timeout
    o = {
      :domain => "none",
      :hosts => %w(0:666 0:6 0:66),
      :connect_timeout => 1
    }
    c = MogileFS::MogileFS.new(o)
    assert_equal 1, c.backend.instance_variable_get(:@connect_timeout)
    o[:timeout] = 5
    c = MogileFS::MogileFS.new(o)
    assert_equal 1, c.backend.instance_variable_get(:@connect_timeout)
    assert_equal 5, c.backend.instance_variable_get(:@timeout)
  end
end
