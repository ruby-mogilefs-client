require "./test/socket_test"
require "mogilefs"
begin
  require "kgio"
  require "mogilefs/socket/kgio"
rescue LoadError
end unless ENV["MOGILEFS_CLIENT_PURE"]

class TestSocketKgio < Test::Unit::TestCase
  include SocketTest
end if defined?(Kgio)
