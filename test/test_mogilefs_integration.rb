# -*- encoding: binary -*-
require './test/fresh'

class TestMogileFSIntegration < Test::Unit::TestCase
  include TestFreshSetup
  def setup
    setup_mogilefs
    add_host_device_domain
    @client = MogileFS::MogileFS.new(:hosts => @trackers, :domain => @domain)
  end

  alias teardown teardown_mogilefs

  def test_CRUD
    assert ! @client.exist?("CRUD")
    assert_equal 4, @client.store_content("CRUD", "default", "DATA")
    assert @client.exist?("CRUD")
    assert_equal 4, @client.size("CRUD")
    assert_equal "DATA", @client.get_file_data("CRUD")
    assert_equal "DAT", @client.get_file_data("CRUD", nil, 3)
    assert_equal "AT", @client.get_file_data("CRUD", nil, 2, 1)

    tmp = Tempfile.new("z")
    tmp_path = tmp.path
    tmp.close!
    assert_equal 4, @client.get_file_data("CRUD", tmp_path)
    assert_equal "DATA", File.read(tmp_path)
    File.unlink(tmp_path)

    sio = StringIO.new("")
    rv = @client.get_file_data("CRUD", sio)
    assert_equal 4, rv
    assert_equal "DATA", sio.string
    assert_equal 8, @client.store_content("CRUD", "default", "MOARDATA")
    assert_equal "MOARDATA", @client.get_file_data("CRUD")
    assert_equal true, @client.delete("CRUD")
    assert_raises(MogileFS::Backend::UnknownKeyError) { @client.delete("CRUD") }

    data = "hello world\n".freeze
    tmp = tmpfile("blob")
    tmp.sync = true
    tmp.write(data)
    tmp.rewind
    assert_equal tmp.size, @client.store_file("blob", nil, tmp)
    assert_equal(data, @client.get_file_data("blob"))

    data = "pipe!\n".freeze
    r, w = IO.pipe
    th = Thread.new do
      w.write(data)
      w.close
    end
    assert_equal data.size, @client.store_file("pipe", nil, r)
    r.close
    th.join
    assert_equal(data, @client.get_file_data("pipe"))

    cbk = MogileFS::Util::StoreContent.new(nil) do |write_callback|
      10.times { write_callback.call("data") }
    end
    assert_nil cbk.length
    nr = @client.store_content('store_content', nil, cbk)
    assert_equal 40, nr
    assert_equal("data" * 10, @client.get_file_data('store_content'))
  end

  def test_store_non_rewindable
    tmp = Object.new
    def tmp.size
      666
    end
    def tmp.read(len, buf = "")
      raise Errno::EIO
    end

    assert_raises(MogileFS::HTTPFile::NonRetryableError) do
      @client.store_file("non_rewindable", nil, tmp)
    end
  end

  def test_file_info
    assert_equal 3, @client.store_content("file_info", "default", "FOO")
    res = @client.file_info("file_info")
    assert_kind_of Integer, res["fid"]
    assert_equal 3, res["length"]
    assert ! res.include?("devids")
    assert_kind_of Integer, res["devcount"]

    res = @client.file_info("file_info", :devices => true)
    assert_kind_of Integer, res["fid"]
    assert_equal 3, res["length"]
    assert_kind_of Integer, res["devcount"]
    devids = res.delete("devids")
    assert_instance_of Array, devids
    devids.each { |devid| assert_kind_of Integer, devid }
    assert_equal res["devcount"], devids.size
  end

  def test_file_debug
    assert_equal 3, @client.store_content("file_debug", "default", "BUG")
    a = @client.file_debug("file_debug")
    b = @client.file_debug(:key => "file_debug")
    fid = @client.file_info("file_debug")["fid"]
    c = @client.file_debug(fid)
    d = @client.file_debug(:fid => fid)

    [ a, b, c, d ].each do |res|
      assert_equal fid, res["fid_fid"]
      assert_equal 0, res["fid_classid"]
      assert_equal "file_debug", res["fid_dkey"]
      assert_equal 3, res["fid_length"]
      assert_kind_of Array, res["devids"]
      assert_kind_of Integer, res["devids"][0]
      res["devids"].each do |devid|
        uri = URI.parse(res["devpath_#{devid}"])
        assert_equal "http", uri.scheme
      end
      assert_equal "default", res["fid_class"]
    end
    @client.delete("file_debug")
    rv = @client.file_debug(fid)
    assert rv.keys.grep(/\Afid_/).empty?, rv.inspect
  end

  def test_file_debug_in_progress
    rv = @client.new_file("file_debug_in_progress") do |http_file|
      http_file << "ZZZZ"
      dests = http_file.instance_variable_get(:@dests)
      dests[0][1] =~ %r{/(\d+)\.fid\z}
      fid = $1.to_i
      rv = @client.file_debug(fid)
      devids = dests.map { |x| x[0].to_i }.sort
      assert_equal devids, rv["tempfile_devids"].sort
      assert_equal "file_debug_in_progress", rv["tempfile_dkey"]
    end
    assert_equal 4, rv
  end

  def test_admin_get_devices
    admin = MogileFS::Admin.new(:hosts => @trackers)
    devices = admin.get_devices
    if any_device = devices[0]
      %w(mb_asof mb_free mb_used mb_total devid weight hostid).each do |field|
        case value = any_device[field]
        when nil
        when Integer
          assert value >= 0, "#{field}=#{value.inspect} is negative"
        else
          assert false, "#{field}=#{value.inspect} is #{value.class}"
        end
      end

      field = "utilization"
      case value = any_device[field]
      when nil
      when Float
        assert value >= 0.0, "#{field}=#{value.inspect} is negative"
      else
        assert false, "#{field}=#{value.inspect} is #{value.class}"
      end
    end
  end

  def test_admin_each_fid
    admin = MogileFS::Admin.new(:hosts => @trackers)
    input = %w(a b c d e)
    input.each do |k|
      rv = @client.new_file(k)
      rv.write(k)
      assert_nil rv.close
    end
    seen = {}
    count = admin.each_fid do |info|
      seen[info["fid"]] = true
      assert_kind_of Integer, info["fid"]
      assert_kind_of Integer, info["length"]
      assert_kind_of Integer, info["devcount"]
      assert_kind_of String, info["key"]
      assert_kind_of String, info["class"]
      assert_kind_of String, info["domain"]
    end
    assert_equal count, seen.size
    assert_equal count, input.size
  end

  def test_new_file_no_block
    rv = @client.new_file("no_block")
    rv.write "HELLO"
    assert_nil rv.close
    assert_equal "HELLO", @client.get_file_data("no_block")
  end

  def test_new_file_known_content_length
    rv = @client.new_file("a", :content_length => 5)
    rv.write "HELLO"
    assert_nil rv.close
    assert_equal "HELLO", @client.get_file_data("a")

    rv = @client.new_file("a", :content_length => 6)
    rv.write "GOOD"
    assert_raises(MogileFS::SizeMismatchError) { rv.close }
    assert_equal "HELLO", @client.get_file_data("a")

    rv = @client.new_file("large", :content_length => 6, :largefile => true)
    assert_instance_of MogileFS::NewFile::Stream, rv
    assert_equal 6, rv.write("HIHIHI")
    assert_nil rv.close
    assert_equal "HIHIHI", @client.get_file_data("large")
  end

  def test_new_file_content_md5
    r, w = IO.pipe
    b64digest = [ Digest::MD5.digest("HELLO") ].pack('m').strip
    rv = @client.new_file("a", :content_md5 => b64digest, :content_length => 5)
    rv.write "HELLO"
    assert_nil rv.close
    assert_equal "HELLO", @client.get_file_data("a")

    w.write "HIHI"
    w.close
    assert_raises(ArgumentError) do
      @client.new_file("a", :content_md5 => b64digest) { |f| f.big_io = r }
    end
    assert_equal "HELLO", @client.get_file_data("a")

    @client.new_file("a", :content_md5 => :trailer) { |f| f.big_io = r }
    assert_equal "HIHI", @client.get_file_data("a")

    # legacy, in case anybody used it
    rv = @client.new_file("a",{:class => "default"}, 6)
    assert_equal 2, rv.write("HI")
    assert_raises(MogileFS::SizeMismatchError) { rv.close }
    assert_equal "HIHI", @client.get_file_data("a")

    rv = @client.new_file("a",{:class => "default"}, 2)
    assert_equal 2, rv.write("HI")
    assert_nil rv.close
    assert_equal "HI", @client.get_file_data("a")
    assert_raises(MogileFS::Backend::UnregClassError) {
      @client.new_file("a", "non-existent", 2)
    }
    assert_raises(MogileFS::Backend::UnregClassError) {
      @client.new_file("a", :class => "non-existent")
    }
  ensure
    r.close if r
  end

  def test_store_content_opts
    b64digest = [ Digest::MD5.digest("HELLO") ].pack('m').strip
    @client.store_content("c", nil, "HELLO", :content_md5 => b64digest)
    assert_raises(MogileFS::SizeMismatchError) do
      @client.store_content("c", nil, "GOODBYE", :content_length => 2)
    end
    assert_equal "HELLO", @client.get_file_data("c")
  end

  def test_store_file_opts
    b64digest = [ Digest::MD5.digest("HELLO") ].pack('m').strip
    io = StringIO.new("HELLO")
    @client.store_file("c", nil, io, :content_md5 => b64digest)

    io = StringIO.new("GOODBYE")
    assert_raises(MogileFS::SizeMismatchError) do
      @client.store_content("c", nil, io, :content_length => 2)
    end
    assert_equal "HELLO", @client.get_file_data("c")
  end

  def test_store_file_content_md5_lambda
    checked = false
    expect_md5 = lambda do
      checked = true
      [ Digest::MD5.digest("HELLO") ].pack('m').strip
    end
    io = StringIO.new("HELLO")
    @client.store_file("c", nil, io, :content_md5 => expect_md5)

    assert_equal true, checked, "expect_md5 lambda called"
  end

  def test_store_file_unlinked_tempfile
    tmp = Tempfile.new("store_file_unlinked")
    tmp.unlink
    tmp.sync = true
    tmp.write "HIHI"
    tmp.rewind
    @client.store_file("unlinked", nil, tmp)

    assert_equal "HIHI", @client.get_file_data("unlinked")
  end

  def test_updateclass
    admin = MogileFS::Admin.new(:hosts => @trackers)
    admin.create_class(@domain, "one", 1)
    admin.create_class(@domain, "two", 2)
    4.times { admin.clear_cache }

    assert_equal 4, @client.store_content("uc", "default", "DATA")
    assert_equal true, @client.updateclass("uc", "one")
    assert_equal true, @client.updateclass("uc", "two")
    assert_raises(MogileFS::Backend::ClassNotFoundError) do
      @client.updateclass("uc", "wtf")
    end
    assert_raises(MogileFS::Backend::InvalidKeyError) do
      @client.updateclass("nonexistent", "one")
    end

    @client.delete "uc"
    admin.delete_class @domain, "one"
    admin.delete_class @domain, "two"
  end
end
