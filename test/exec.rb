# -*- encoding: binary -*-
$stdout.sync = $stderr.sync = true
Thread.abort_on_exception = true
require 'test/unit'
require 'securerandom'
require 'tempfile'
require 'digest'
require 'stringio'
require 'pp'
require 'mogilefs'

module TestExec
  def uuid
    SecureRandom.uuid
  rescue NoMethodError
    ary = SecureRandom.random_bytes(16).unpack("NnnnnN")
    ary[2] = (ary[2] & 0x0fff) | 0x4000
    ary[3] = (ary[3] & 0x3fff) | 0x8000
    "%08x-%04x-%04x-%04x-%04x%08x" % ary
  end

  def yield_for_monitor_update # mogilefsd should update every 4 seconds
    50.times do
      yield
      sleep 0.1
    end
  end

  def mogadm(*args)
    x("mogadm", "--trackers=#{@trackers.join(',')}", *args)
  end

  def x(*cmd)
    out, err = tmpfile("out"), tmpfile("err")
    puts cmd.join(' ') if $VERBOSE
    pid = fork do
      $stderr.reopen(err.path, "a")
      $stdout.reopen(out.path, "a")
      out.close
      err.close
      ObjectSpace.each_object(Tempfile) do |tmp|
        next if tmp.closed?
        ObjectSpace.undefine_finalizer(tmp)
        tmp.close_on_exec = true if tmp.respond_to?(:close_on_exec=)
      end
      exec(*cmd)
    end
    _, status = Process.waitpid2(pid)
    out.rewind
    err.rewind
    [ status, out, err ]
  end

  def mogadm!(*args)
    status, out, err = mogadm(*args)
    assert status.success?, "#{status.inspect} / #{out.read} / #{err.read}"
    [ status, out, err ]
  end

  def x!(*cmd)
    status, out, err = x(*cmd)
    assert status.success?, "#{status.inspect} / #{out.read} / #{err.read}"
    [ status, out, err ]
  end

  def tmpfile(name)
    tmp = Tempfile.new(name)
    defined?(@to_close) or @to_close = []
    @to_close << tmp
    tmp
  end
end
